# Todo

* [x] ne cacher que les images de moins de Xko
* [x] config separee strategie de cache html vs ressources
* [x] gerer le cas insertion auto si on est connecte uniquement
* [x] cacher une version sans stimetamp des ressources css/js et les utiliser en fallback quand on cherche une autre version du meme fichier
* [x] fonction de parsing d'une URL pour trouver toutes les ressources
* [x] tester toutes les URLs parsees pour ne garder que celles qui répondent en 2xx 
* [x] mise en cache du service worker et de l'install
* [x] gérer la désinstallation du serviceworker
* [x] build du service worker en spip-cli
* [x] prompt pour pesistence du storage quand l'utilisateur demande a stocker du contenu pour plus tard
* [x] mise en cache d'une liste d'URL sur demande de l'utilisateur
* [x] build du service worker lorsqu'on enregistre la configuration
* [x] spip-cli de rebuild complet pour appeler en cron
* [x] permettre de mettre un tag dans une url d'image (?1234&offline-cache) pour forcer la mise en cache même si taille supérieure au seuil