<?php
/**
 * Définit les autorisations du plugin Offline
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function offline_autoriser() {
}

function autoriser_offline_configurer_dist($faire, $quoi, $id, $qui, $options) {
	return autoriser('webmestre');
}