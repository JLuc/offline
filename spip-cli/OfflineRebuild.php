<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class OfflineRebuild extends Command {
	protected function configure() {
		$this
			->setName('offline:rebuild')
			->setDescription('Reconstuire le service worker et toutes les listes d\'URLs des objets téléchargeables offline')
			->addOption(
				'refresh',
				null,
				InputOption::VALUE_OPTIONAL,
				'Forcer la mise a jour du contenu des URLs avant de les parser',
				false
			)
		;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('inc/offline');
		global $spip_racine;
		global $spip_loaded;

		$refresh = $input->getOption('refresh');
		$option = "";
		if ($refresh) {
			$option = " --refresh";
		}

		$this->io->comment("Rebuild des services");
		passthru('spip offline:build:services' . $option);

		$files = glob(_DIR_ETC . 'offline/objets/urls-*.txt');
		foreach ($files as $file) {
			if (preg_match(",^urls-(\w+)-(\d+)\.txt$,", basename($file), $m)) {
				$objet = $m[1];
				$id_objet = $m[2];
				$this->io->comment("Rebuild des URLS $objet #$id_objet");
				passthru("spip offline:build:urls --objet=$objet --id_objet=$id_objet" . $option);
			}
		}

		$this->io->info("Rebuild complet");
		return Command::SUCCESS;

	}
}
