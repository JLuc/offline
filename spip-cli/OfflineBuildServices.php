<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class OfflineBuildServices extends Command {
	protected function configure() {
		$this
			->setName('offline:build:services')
			->setDescription('Constuire le service worker')
			->addOption(
				'refresh',
				null,
				InputOption::VALUE_OPTIONAL,
				'Forcer la mise a jour du contenu des URLs avant de les parser',
				false
			)
		;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('inc/offline');
		include_spip('offline_fonctions');
		global $spip_racine;
		global $spip_loaded;

		$refresh = $input->getOption('refresh');

		if (offline_build_services($refresh)) {
			$config = offline_config_js();

			include_spip('inc/texte');
			include_spip('inc/print');
			$this->io->text(OfflineBuildServices::printConfig($config));
			$this->io->text(offline_services_last_version());
			$this->io->info("Service buildé");
			return Command::SUCCESS;
		}
		else {
			$this->io->error("Erreur build incomplet");
			return Command::FAILURE;
		}

	}

	public static function printConfig($u, $join = "\n", $indent = 0) {
		if (is_string($u)) {
			return $u;
		}

		// caster $u en array si besoin
		if (is_object($u)) {
			$u = (array)$u;
		}

		if (is_array($u)) {
			$out = "";
			// sinon on passe a la ligne et on indente
			$i_str = str_pad("", $indent, " ");

			// toutes les cles sont numeriques ?
			// et aucun enfant n'est un tableau
			// liste simple separee par des virgules
			$numeric_keys = array_map('is_numeric', array_keys($u));
			$array_values = array_map('is_array', $u);
			$object_values = array_map('is_object', $u);
			if (array_sum($numeric_keys) == count($numeric_keys)
				and !array_sum($array_values)
				and !array_sum($object_values)
			) {
				foreach ($u as $v) {
					$out .= $join . $i_str . "" . $v;
				}
			}
			else {
				foreach ($u as $k => $v) {
					$out .= $join . $i_str . "$k: " . OfflineBuildServices::printConfig($v, $join, $indent + 2);
				}
			}

			return $out;
		}

		// on sait pas quoi faire...
		return $u;
	}

}
