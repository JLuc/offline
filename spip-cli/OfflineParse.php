<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class OfflineParse extends Command {
	protected function configure() {
		$this
			->setName('offline:parse')
			->setDescription('Lancer l’indexation des contenus SPIP configurés.')
			->addOption(
				'url',
				null,
				InputOption::VALUE_REQUIRED,
				'URL a parser pour trouver les ressources associees',
				null
			)
			->addOption(
				'refresh',
				null,
				InputOption::VALUE_OPTIONAL,
				'Forcer la mise a jour du contenu des URLs avant de les parser',
				false
			)
			->addOption(
				'profondeur',
				null,
				InputOption::VALUE_OPTIONAL,
				'Profondeur de suivi descente dans les ressources a parser',
				5
			)
		;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('inc/offline');
		global $spip_racine;
		global $spip_loaded;

		$url = $input->getOption('url');
		$refresh = $input->getOption('refresh');
		$profondeur = $input->getOption('profondeur');

		$ressources = offline_ressources_from_url($url, $refresh, $profondeur);

		$this->io->text($ressources);
		return Command::SUCCESS;
	}
}
