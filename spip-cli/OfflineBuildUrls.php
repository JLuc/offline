<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class OfflineBuildUrls extends Command {
	protected function configure() {
		$this
			->setName('offline:build:urls')
			->setDescription('Constuire les URLs de téléchargement offline d\'un objet')
			->addOption(
				'objet',
				null,
				InputOption::VALUE_REQUIRED,
				'objet',
				null
			)
			->addOption(
				'id_objet',
				null,
				InputOption::VALUE_REQUIRED,
				'id_objet',
				null
			)
			->addOption(
				'refresh',
				null,
				InputOption::VALUE_OPTIONAL,
				'Forcer la mise a jour du contenu des URLs avant de les parser',
				false
			)
		;
	}


	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('inc/offline');
		global $spip_racine;
		global $spip_loaded;

		$objet = $input->getOption('objet');
		$id_objet = $input->getOption('id_objet');
		$refresh = $input->getOption('refresh');

		if (!$objet or !$id_objet) {
			$this->io->error("Indiquez --objet= et --id_objet= pour builder les URLs");
			return Command::FAILURE;
		}

		$file_urls_load = offline_filename_urls_to_load_objet($objet, $id_objet);
		if ($urls = offline_build_urls_objet($objet, $id_objet, $refresh)) {
			$this->io->text(implode("\n", $urls));
			$this->io->info("URLs $objet $id_objet buildées dans $file_urls_load");
			return Command::SUCCESS;
		}
		else {
			$this->io->error("Erreur build incomplet pour $objet $id_objet ($file_urls_load)");
			return Command::FAILURE;
		}

	}
}
