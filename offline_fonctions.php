<?php
/**
 * Fonctions utiles au plugin Offline
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function offline_building_services_in_progress() {
	$file_in_progress = _DIR_ETC . 'offline/building.tmp';
	if (file_exists($file_in_progress)) {
		return date('H:i:s',filemtime($file_in_progress));
	}
	return '';
}

function offline_services_last_version() {
	$file_in_progress = _DIR_ETC . 'offline/building.tmp';
	if (file_exists($file_in_progress)) {
		return '';
	}

	include_spip('inc/offline');
	if (defined('_OFFLINE_DEBUG') and _OFFLINE_DEBUG) {
		$fichiers = array(
			_DIR_ETC . 'offline/urls_to_cache.txt',
		);
		$t = null;
		foreach ($fichiers as $f) {
			if (file_exists($f) and $modified = filemtime($f)) {
				if (is_null($t) or $modified>$t) {
					$t = $modified;
				}
			}
		}

		if ($t) {
			return date('Y-m-d H:i:s',$t);
		}

	}
	else {
		$fichiers = array(
			_DIR_VAR . 'offline/install.js',
			_DIR_VAR . 'offline/uninstall.js',
			_DIR_VAR . 'offline/sw.js',
		);

		$t = array();
		foreach ($fichiers as $f) {
			if (file_exists($f)) {
				list($version, $file_version) = offline_last_service_version($f);
				$t[] = basename($f) . '->' . basename($file_version);
			}
		}
		if ($t) {
			return "\n- " . implode("\n- ", $t);
		}
	}

	return '';
}

/**
 * Construire les services, en tache de fond
 * @param bool $force_refresh
 * @param int $iteration
 */
function offline_start_build_services($force_refresh=false) {
	// on touch pour l'interface dans la page de configuration
	$dir_config = sous_repertoire(_DIR_ETC, 'offline');
	$file_in_progress = $dir_config . "building.tmp";
	@touch($file_in_progress);

	job_queue_add('offline_background_build_services', "Build Services offline", array($force_refresh), 'inc/offline', true);
}

/**
 * Lancer le build en tache de fond des URLs a telecharger pour un objet offline
 * @param string $objet
 * @param int $id_objet
 * @param bool $force_refresh
 */
function offline_start_build_urls_objet($objet, $id_objet, $force_refresh=false) {
	job_queue_add('offline_background_build_urls_objet', "Build URLs offline $objet-$id_objet", array($objet, $id_objet, $force_refresh), 'inc/offline', true);
}


/**
 * Inserer des ressources supplémentaires dans le head d'une page existante
 * (pour adapter la 404 de base en 404 offline)
 * @param string $page_html
 * @param string $insert
 * @return mixed
 */
function offline_inserer_head($page_html, $insert) {
	if ($p = stripos($page_html, '</head>')) {

		if ($p1 = stripos($page_html, '<link') and $p1<$p) {
			$p = $p1;
		}
		if ($p1 = stripos($page_html, '<script') and $p1<$p) {
			$p = $p1;
		}
		
		$insert = "\n" . trim($insert) . "\n";
		$page_html = substr_replace($page_html, $insert, $p, 0);
	}
	return $page_html;
}

/**
 * #BOUTON_TELECHARGER_OFFLINE{rubrique,#ID_RUBRIQUE,'Télécharger cette documentation','Téléchargement non disponible'}
 * @param Champ $p
 * @return Champ
 */
function balise_BOUTON_TELECHARGER_OFFLINE_dist($p) {

	$_objet = interprete_argument_balise(1, $p);
	$_id_objet = interprete_argument_balise(2, $p);
	$_label_active = "''";
	$_label_unactive = "''";
	if (($v = interprete_argument_balise(3, $p)) !== null) {
		$_label_active = $v;
		if (($v = interprete_argument_balise(4, $p)) !== null) {
			$_label_unactive = $v;
		}

	}

	$p->code = "offline_bouton_telecharger($_objet,$_id_objet,$_label_active,$_label_unactive)";
	$p->interdire_scripts = false;
	return $p;
}


/**
 * Filtre d'affichage utilise par #BOUTON_TELECHARGER_OFFLINE
 * @param string $objet
 * @param int $id_objet
 * @param string $label_active
 * @param string $label_unactive
 * @return string
 */
function offline_bouton_telecharger($objet, $id_objet, $label_active='', $label_unactive='') {

	// est-ce qu'on a bien un fond disponible pour lister les pages a telecharger ?
	if (!trouver_fond('offline/urls-'.$objet)) {
		return '';
	}

	include_spip('inc/offline');
	// 3 status possibles :
	// unactive : le telechargement n'est pas disponible pour cet objet (ou pour le moment car pas de build des urls dispo)
	// activable : tout est OK cote serveur, le JS devra verifier cote client
	// active : le bouton est actif et permet le telechargement
	$status = 'activable'; // pas 'active' par defaut, car c'est le JS qui activera si le service-worker est bien charge
	// est-ce qu'on a bien une collection buildee pour cet objet, ou faut-il lancer le build en cron ?
	if (!offline_urls_to_load_objet($objet, $id_objet, _VAR_MODE ? 'refresh' : false)) {
		$status = 'unactive';
	}

	$contexte = array(
		'objet' => $objet,
		'id_objet' => $id_objet,
		'status' => $status,
		'label_active' => $label_active,
		'label_unactive' => $label_unactive,
	);
	$html = recuperer_fond('offline/bouton-telecharger', $contexte);

	return $html;
}