<?php
/**
 * Formulaire de configuration
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * traiter le formulaire de configuration : traitement par defaut + lancer le cron de reconstruction des services
 * @return string
 */
function formulaires_configurer_offline_traiter_dist() {
	include_spip('inc/cvt_configurer');
	$args = func_get_args();
	$res = cvtconf_formulaires_configurer_enregistre('configurer_offline', $args);

	// lancer la reconstruction du service
	include_spip('offline_fonctions');
	offline_start_build_services();

	return $res;
}