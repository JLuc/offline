offline.cacheClearOld = function(raison) {
	var cacheWhitelist = [ offlineConfig.cacheName ];

	return caches.keys().then(function(cacheNames) {
		return Promise.all(
			cacheNames.filter(function (cacheName){
				// Return true if you want to remove this cache,
				// but remember that caches are shared across
				// the whole origin
				// remove caches beginning "offline-" that aren't in
				// cacheWhitelist
				if (/^offline-/.test(cacheName)
					&& cacheWhitelist.indexOf(cacheName)=== -1){
					return true;
				}
			}).map(function (cacheName){
				offline.log(raison + ":cacheClearOld:"+cacheName);
				return caches.delete(cacheName);
			})
		)
	})
}

offline.cacheAddAll = function(raison, urls) {

	return caches.open(offlineConfig.cacheName)
   .then(function(cache) {
     offline.log(raison + ':cacheAddAll');
     return cache.addAll(urls);
   })

}

offline.destinationMedias = [ 'image', 'audio', 'video' ];
offline.cachePut = function(raison, request, response) {

	return caches.open(offlineConfig.cacheName)
		.then(function(cache) {
			// don't cache fails
			if (!response || response.status >= 400) {
				return;
			}

			if (/[\?&]var_mode=/.test(request.url)) {
				if (/[\?&]var_mode=(re)?calcul/.test(request.url)) {
					// remove var_mode arg from the url
					var url = request.url;
					url = url.replace(/([&?])var_mode=\w+$/,"");
					url = url.replace(/[&?]var_mode=\w+&/,"$1");
					request = new Request(url);
					offline.log(raison + ":cachePut:var_mode:removing", request.url);
				}
				else {
					// no caching if var_mode and not in calcul|recalcul
					offline.log(raison + ":cachePut:var_mode:no-caching", request.url);
					return;
				}
			}
			// if this is an image or media, cache only if small enough
			if (offlineConfig.mediaMaxCacheSize) {
				// si l'url du media contient ?offline-cache ou &offline-cache on le met en cache s'en s'occuper de la taille
				if (request.url.indexOf("offline-cache") == -1) {
					if (offline.destinationMedias.indexOf(request.destination)!==-1
					 || (   response.headers.has('content-type')
					     && response.headers.get('content-type').match(/^(image|audio|video)\//i))
					) {
						var length = response.headers.get("content-length");
						if (!length || typeof length === "undefined") {
							offline.log(raison + ":cachePut:media:caching:length-unknown", request.url);
						}
						else if (length>offlineConfig.mediaMaxCacheSize) {
							offline.log(raison + ":cachePut:media:no-caching:length>" + offlineConfig.mediaMaxCacheSize, request.url);
							return;
						}
						else {
							offline.log(raison + ":cachePut:media:caching:length<" + offlineConfig.mediaMaxCacheSize, request.url);
						}
					}
				}
			}

			offline.log(raison + ":cachePut:", request.url);
			cache.put(request, response);

			// if this is a css/js with a timestamp store into the url without timestamp, as a generic fallback
			if (/\.(css|js)\?\d+$/.test(request.url)) {
				var url = request.url;
				url = url.replace(/\?\d+$/, "");
				request = new Request(url);
				offline.log(raison + ":cachePut:no-timestamp:", request.url);
				cache.put(request, response);
			}
   });

}