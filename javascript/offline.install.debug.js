offline.showSWStatut = function() {
	var c = navigator.serviceWorker.controller;
	var status = "DEBUG SW: ";
	if (!jQuery('#swstatus').length) {
		jQuery("<div id='swstatus' style='position:fixed;top:0;left:0;background:#eee;padding:5px 10px'></div>").appendTo('body');
	}
	if (typeof c !== "undefined" && c) {
		status = status + 'ON ' + c.state;
		jQuery('#swstatus').html(status);
		if (typeof navigator.storage !== "undefined" && navigator.storage) {
			navigator.storage.estimate().then(info => {
				var mb,s;
				mb = Math.round(info.usage/1024/1024*10) / 10;
				s = mb + 'Mb';
				mb = Math.round(info.quota/1024/1024);
				s = s + '/' + mb + 'Mb';
				jQuery('#swstatus').append(' | ' + s);
			});
		}
	}
	else {
		status = status + '-';
		jQuery('#swstatus').html(status);
	}
}