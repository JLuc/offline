offline.manager = function (request){

	var strategy;

	if (request.mode==='navigate'){
		console.log('Offline SW version ' + offline['sw.js']);
		console.log({cacheName:offlineConfig.cacheName,strategyNav:offlineConfig.strategyNav,strategyRes:offlineConfig.strategyRes,mediaMaxCacheSize:offlineConfig.mediaMaxCacheSize});
		// default strategy
		strategy = offlineConfig.strategyNav;

		// main request (page)
		offline.log("manager:navigate:"+request.method+':', request.url);
		if (offline.forceRefresh){
			offline.forceRefresh = false;
		}
		// if var_mode, force the refresh
		if (/[\?&]var_mode=\w/.test(request.url)
		){
			offline.forceRefresh = true;
			offline.log("manager:navigate:forceRefresh", offline.forceRefresh);
		}
	}
	else {
		// default strategy for resources
		strategy = offlineConfig.strategyRes;

		offline.log("manager:resource:" + request.method + ':', request.url);
		// known from statics folders or timestamped are taken from cache without revalidation
		// ?var_mode=calcul on the main page url is forcing a refresh
		if (/\/(IMG|local|plugins|plugins-dist|squelettes|squelettes-dist)\//.test(request.url)
			|| /\.(png|gif|jpg|js|css)\?\d+$/.test(request.url)){
			if (strategy === offline._STALE_WHILE_REVALIDATE) {
				strategy = offline._CACHE_FALLING_BACK_TO_NETWORK;
			}
		}

	}

	// if POST request
	// + dans l'espace prive, on ne cache aucune requete
	if (request.method=='POST'
		|| /\/(ecrire\/|\w+.api[.\/])/.test(request.url)
		|| /[\?&]action=\w/.test(request.url)
	){
		strategy = offline._NETWORK_ONLY;
	}

	// if var_mode or was on last main request
	// let's force a refresh from network
	if (offline.forceRefresh && strategy!==offline._NETWORK_ONLY){
		//offline.log("manager:strategy force:" + offline._NETWORK_FALLING_BACK_TO_CACHE + ':', request.url);
		strategy = offline._NETWORK_FALLING_BACK_TO_CACHE;
	}

	//offline.log("manager:strategy:" + strategy + ':', request.url);
	return strategy;
}