var offline = {
	version: 0, // utilisable en debug pour incrementer et verifier la mise a jour du service
	'sw.js':0, // surcharge en prod lors de l'ecriture du fichier statique pour afficher un numero de version coherent avec celui du fichier
	/* Flag pour forcer la mise a jour sur un ?var_mode, jusqu'a la requete principale suivante */
	forceRefresh: false,

	_NETWORK_ONLY: 1,
	_NETWORK_FALLING_BACK_TO_CACHE: 2,
	_CACHE_FALLING_BACK_TO_NETWORK: 4,
	_STALE_WHILE_REVALIDATE: 8,

	log: function (text){
		if (!offlineConfig.debug) return;
		var t = new Date();
		if (console && console.log){
			var pre = "ofl-v"+offline.version+":"+t.getMilliseconds()+":";
			var a = Array.from(arguments);
			a.unshift(pre);
			console.log.apply(console, a);
		}
	},

	fallback: function (request){
		offline.log("fallback:", request.url);
		if (/\.(css|js)\?\d+$/.test(request.url)){
			var url = request.url;
			url = url.replace(/\?\d+$/, "");
			offline.log("fallback:->url:", url);
			request = new Request(url);
			return caches.match(url).then(
				function (response){
					if (!response || response===undefined){
						return offline.fallback(request);
					}
					return response;
				}
			)
		}

		if (/\.(js|css)($|\?)/.test(request.url)){
			return new Response("/* Offline error - connexion failed */", {status: 503});
		}
		if (/\.(ico)($|\?)/.test(request.url)){
			return new Response("", {status: 503});
		}

		var url = offlineConfig.urlsFallback.default;
		if (/\.(png|gif|jpg)($|\?)/.test(request.url)){
			url = offlineConfig.urlsFallback.image;
		}
		offline.log("fallback:->url:", url);

		// If both fail, show a generic fallback:
		return caches.match(url).then(
			function (response){
				//offline.log("fallback:response:", response);
				if (!response || response===undefined){
					return new Response("/* Offline error - connexion failed */", {
						status: 503
					});
				}
				return response;
			}
		)
		// However, in reality you'd have many different
		// fallbacks, depending on URL & headers.
		// Eg, a fallback silhouette image for avatars.
	},

	fetchAndCache: function (raison, request){
		// IMPORTANT: Clone the request. A request is a stream and
		// can only be consumed once. Since we are consuming this
		// once by cache and once by the browser for fetch, we need
		// to clone the response.
		var fetchRequest = request.clone();

		return fetch(fetchRequest, {credentials: 'include'}).then(
			function (response){
				// Check if we received a valid response
				if (!response || response.status!==200 || response.type!=='basic'){
					offline.log(raison+":invalide-response:", request.url);
					return response;
				}

				// IMPORTANT: Clone the response. A response is a stream
				// and because we want the browser to consume the response
				// as well as the cache consuming the response, we need
				// to clone it so we have two streams.
				var responseToCache = response.clone();
				offline.cachePut(raison+':fetchAndCache', request, responseToCache);
				offline.log(raison+":return-fetched:", request.url);
				return response;
			}
		);
	},

	networkOnly: function (event){
		// IMPORTANT: Clone the request. A request is a stream and
		// can only be consumed once. Since we are consuming this
		// once by cache and once by the browser for fetch, we need
		// to clone the response.
		var fetchRequest = event.request.clone();
		return fetch(fetchRequest, {credentials: 'include'}).then(
			function (response){
				offline.log("networkOnly:return-fetched:", event.request.url);
				return response;
			}
		);
	},

	netWorkFallingBackToCache: function (event){
		return offline.fetchAndCache('netWorkFallingBackToCache', event.request)
			.catch(function (){
				return caches.match(event.request)
					.then(function (response){
						if (response){
							offline.log("netWorkFallingBackToCache:return-cache-hit:", event.request.url);
							return response;
						}
						offline.log("netWorkFallingBackToCache:fail:", event.request.url);
						return offline.fallback(event.request);
					})
			})
	},

	cacheFallingBackToNetwork: function (event){
		return caches.match(event.request)
			.then(function (response){
				// Cache hit - return response
				if (response){
					offline.log("cacheFallingBackToNetwork:return-cache-hit:", event.request.url);
					return response;
				}

				return offline.fetchAndCache('cacheFallingBackToNetwork', event.request)
					.catch(function (){
						offline.log("cacheFallingBackToNetwork:fail:", event.request.url);
						return offline.fallback(event.request);
					});
			})
	},

	staleWhileRevalidate: function (event){
		return caches.match(event.request)
			.then(function (response){
				// Cache hit - return response but run a fetch before for updating the cache
				if (response){
					offline.log("staleWhileRevalidate:return-cache-hit:", event.request.url);
					offline.fetchAndCache('staleWhileRevalidate:fetch-for-cache', event.request)
					return response;
				}

				return offline.fetchAndCache('staleWhileRevalidate:fetch-fallback', event.request)
					.catch(function (){
						offline.log("staleWhileRevalidate:fail:", event.request.url);
						return offline.fallback(event.request);
					});
			})
	}

}
