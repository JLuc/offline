if ('serviceWorker' in navigator){
	if ('getRegistrations' in navigator.serviceWorker){
		navigator.serviceWorker.getRegistrations().then(function (registrations){
			for (let registration of registrations){
				registration.unregister()
			}
			console.log("offline: serviceWorkers unregistered")
		})
	}
	else if ('getRegistration' in navigator.serviceWorker){
		navigator.serviceWorker.getRegistration().then(function (registration){
			registration.unregister();
			console.log("offline: serviceWorker unregistered")
		})
	}
}
