var offlineConfig;
if (typeof offlineConfig=="undefined"){
	console.error("Erreur offline.install.js appele sans configuration");
}
else {
	if ('serviceWorker' in navigator){
		window.addEventListener('load', function (){
			offline.activeDownloads();
			navigator.serviceWorker.register(offlineConfig.swUrl, offlineConfig.swOptions).then(function (registration){
				// Registration was successful
				console.log('ServiceWorker registration successful with scope: ', registration.scope);
			}, function (err){
				// registration failed :(
				console.log('ServiceWorker registration failed: ', err);
			});
		});
	}
}
