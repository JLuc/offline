/**
 * Configuration du service worker et de son installation
 * Ce fichier sert uniquement de documentation
 * La configuration dynamique est fournie par /offline.api?q=config.js
 * @type {{swUrl: string, swOptions: {}, cacheName: string, urlsToCache: string[]}}
 */
var offlineConfig = {
	// url du service worker que l'on installe
	swUrl: "",
	// options d'installation
	swOptions: {},
	// nom du cache actif
	cacheName: "offline-012345789",
	// mode debug
	debug: false,
	// Strategie d'utilisation du cache
	/*
	_NETWORK_ONLY : 1,
	_NETWORK_FALLING_BACK_TO_CACHE : 2,
	_CACHE_FALLING_BACK_TO_NETWORK : 4,
	_STALE_WHILE_REVALIDATE : 8,
	 */
	strategy: 2,
	// taille maxi des media mises en cache (0 = pas de maxi)
	mediaMaxCacheSize:0,
	// urls a mettre en cache a l'installation du service worker
	urlsToCache: [
		'/',
		'/?page=404_offline',
		'img/fallback.png'
	],
	// urls de fallback selon le type de fichier
	urlsFallback: {
		image: 'img/fallback.png',
		default: "/?page=404_offline"
	}

}
