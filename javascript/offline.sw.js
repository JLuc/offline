/**
 * Only core essential logic of the service worker has to be here
 * if a single byte is changed the service worker is reloaded and reinstalled, flushing the previous one
 * So all generic functions are in offline.utils.js which is included in the page, not in the sw
 */
var offlineConfig;
if (typeof offlineConfig=="undefined"){
	console.error("Erreur offline.service.js appele sans configuration");
}

self.addEventListener('install', function (event){
	offline.log("sw.install", offlineConfig);

	// Perform install steps
	event.waitUntil(offline.cacheAddAll('sw.install', offlineConfig.urlsToCache));
});

self.addEventListener('activate', function (event){

	offline.log("sw.activate:cachename:", offlineConfig.cacheName);

	event.waitUntil(offline.cacheClearOld('sw.activate'));
});

self.addEventListener('fetch', function (event){

	var strategy = offline.manager(event.request);

	/**
	 * Securite pour pouvoir detruire ce service worker dans le futur
	 */
	if (event.request.url.indexOf('?service-worker-auto-destruction') !== -1) {
		self.registration.unregister();
		console.log("offline: serviceWorker unregistered");
		event.respondWith(
			new Response("Service worker unregistered - bye", {
				status: 512
			})
		);
		return;
	}

	switch (strategy) {
		case offline._NETWORK_ONLY:
			if (offlineConfig.debug) {
				// for debug use offline.networkOnly
				event.respondWith(offline.networkOnly(event));
			}
			// or simply don't call event.respondWith, which
			// will result in default browser behaviour
			break;
		case offline._NETWORK_FALLING_BACK_TO_CACHE:
			event.respondWith(offline.netWorkFallingBackToCache(event));
			break;
		case offline._CACHE_FALLING_BACK_TO_NETWORK:
			event.respondWith(offline.cacheFallingBackToNetwork(event));
			break;
		case offline._STALE_WHILE_REVALIDATE:
		default:
			event.respondWith(offline.staleWhileRevalidate(event));
			break;
	}

});
