jQuery.fn.checkOfflineStatus = function() {
	return this.each(function() {
		var me = jQuery(this);
		var url = me.attr('data-src');
		url = parametre_url(url, 'var_mode','loaded');
		console.log('check offline '+url);
		caches.match(url).then(
			function (response){
				if (response && response!==undefined){
					me.closest('.offline-download').removeClass('offline-error').addClass('offline-ok');
				}
			}
		)
	});
};
jQuery.fn.setOfflineDownloadingStatus = function(downloading) {
	return this.each(function() {
		var me = jQuery(this).closest('.offline-download');
		if (downloading) {
			me.addClass('offline-downloading')
				.removeClass('offline-ok')
				.removeClass('offline-error')
				.find(':active,:focus').each(function(){this.blur()});
		}
		else {
			me.removeClass('offline-downloading').find(':active,:focus').each(function(){this.blur()});
		}
	});
};
jQuery.fn.setOfflineErrorStatus = function() {
	return this.each(function() {
			var me = jQuery(this).closest('.offline-download');
			me.addClass('offline-error');
		})
}
var offline = {
  activeDownloads : function() {
    var c = navigator.serviceWorker.controller;
   	if (typeof c!=="undefined" && c){
   		jQuery('.offline-download.activable')
		    .removeClass('activable')
		    .addClass('active')
		    .find('.btn-download').on('click', offline.downloadURLs)
		    .checkOfflineStatus()
	    ;
   	}
   	else {
   		jQuery('.offline-download.activable').removeClass('activable').addClass('unactive');
   	}
	  if (typeof offline.showSWStatut === 'function') {offline.showSWStatut();}
  },
	downloadURLs: function(event) {
		event.preventDefault();
		var me = jQuery(this);
		var src = me.attr('data-src');
		offline.promptPersistent();
		me.setOfflineDownloadingStatus(true);
		caches.open(offlineConfig.cacheName).then(function(cache) {
			fetch(src).then(function(response) {
				// put this in cache then we know this is already downloaded for offline
				console.log('ofl-download:got response '+src);
				// src returns a JSON-encoded array of
				// resource URLs that a given object depends on
				//console.log(response.json());
				return response.json().then(function(urls){
					console.log('ofl-download:addAll:' + urls.length + " urls");
					cache.addAll(urls).then(function() {
						src = parametre_url(src, 'var_mode','loaded');
						cache.put(src,new Response("OK", {status: 200})).then(function(){
							console.log('ofl-download:cache.put ok');
							//console.log('ofl-download:addAll finished');
							me.setOfflineDownloadingStatus(false);
							me.checkOfflineStatus();
						});
					})
					.catch(function (error){
						// catch seems to not work if 404 in the urls and addAll fail
						offline.log("ofl-download:errors:", error);
						me.setOfflineErrorStatus();
					});
					//console.log('ofl-download:addAll after');
				});
			});
		});
	},
  promptPersistent : function() {
    // https://storage.spec.whatwg.org/
	  if (typeof navigator.storage !== "undefined" && navigator.storage) {
		  Promise.all([
      navigator.storage.persisted(),
      navigator.permissions.query({name: "persistent-storage"})
	    ]).then(([persisted, permission]) => {
	      if(permission.state == "granted") {
	        // nothing to do !
	      } else if(permission.state == "prompt") {
	        // ask once
			    navigator.storage.persist();
	      } else if(permission.state == "denied") {
	        // tant pis ?
	      }
	      //console.log(persisted,permission);
	    })
	  }
  }
}
