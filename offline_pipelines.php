<?php
/**
 * Utilisations de pipelines par Offline
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Inserer le code d'install du service worker dans la page
 * @param string $flux
 * @return string
 */
function offline_insert_head($flux) {

	if (isset($GLOBALS['meta']['offline']) and
		$config = unserialize($GLOBALS['meta']['offline'])) {

		if (isset($config['mode']) and in_array($config['mode'], array('auto_all', 'auto_logged'))) {

			$api_offline = charger_fonction('api_offline', 'action');
			if ($script = $api_offline('install.js', true, 'cache')) {
				$script = "<script type='text/javascript'>$script</script>\n";
				if ($config['mode'] == 'auto_logged') {
					$script = "<" . "?php if (isset(\$GLOBALS['visiteur_session']['id_auteur']) and intval(\$GLOBALS['visiteur_session']['id_auteur'])) { ?" .">\n"
						. $script
						. "<" . "?php } ?" . ">\n";
				}
				$flux .= $script;
			}
		}

		if (isset($config['mode']) and $config['mode'] === 'off') {
			$api_offline = charger_fonction('api_offline', 'action');
			if ($script = $api_offline('uninstall.js', true, 'cache')) {
				$script = "<script type='text/javascript'>$script</script>\n";
				$flux .= $script;
			}
		}
	}

	return $flux;
}
