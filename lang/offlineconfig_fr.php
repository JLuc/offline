<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'explication_version_cache_edito' => 'Version éditoriale. Le changement de numéro de version force la mise à jour de toutes les pages en cache chez tous les visiteurs.',
	'explication_cacher_ressources' => 'URLs des ressources supplémentaires à mettre en cache par défaut (une url par ligne)',
	'explication_url_offline_404' => 'URL de la page présentée quand un contenu n\'est pas disponible hors connexion',
	'explication_strategie_cache_res' => 'Concerne les images, CSS, JS, Fonts…',
	'explication_cacher_ressources' => 'Par défaut la home page et l\'URL 404 offline du site ainsi que leurs ressources seront mise en cache lors de l\'activation. 
<br />Si certaines ressources spécifiques manquaient ou si vous voulez mettre en cache d\'autres pages par défaut, indiquez les URLs ici',

	'info_building_in_progress' => 'Le service worker est en cours de mise à jour…',
	'info_service_version' => 'Dernière version du service :',

	'label_version_cache_edito' => 'Version du cache',
	'label_mode' => 'Activation du mode offline',
	'label_mode_auto_all' => 'Activer le mode offline pour tous les visiteurs',
	'label_mode_auto_logged' => 'Activer le mode offline pour les visiteurs connectés',
	'label_mode_manual' => 'Pas d\'activation automatique (activation manuelle à gérer par un script)',
	'label_mode_off' => 'Désinstaller le mode offline chez les visiteurs (désactivation)',
	'label_url_offline_404' => 'URL 404 offline',

	'label_strategie_cache_nav' => 'Stratégie pour les <b>pages</b>',
	'label_strategie_cache_nav_cache_first' => '<i lang="en">cache-first</i> : Servir la page en cache si dispo, et mise à jour avec la connexion pour la prochaine fois (optimise le <b>confort</b>)',
	'label_strategie_cache_nav_network_first' => '<i lang="en">network-first</i> : Essayer de charger la page avec la connexion, utiliser le cache en cas d\'échec (optimise la <b>fraicheur du contenu</b>)',

	'label_strategie_cache_res' => 'Stratégie pour les <b>ressources</b>',
	'label_strategie_cache_res_cache_first' => '<i lang="en">cache-first</i> : Servir la ressource en cache si dispo, utiliser le réseau en cas d\'échec (optimise la <b>rapidité</b>)',
	'label_strategie_cache_res_network_first' => '<i lang="en">network-first</i> : Essayer de charger la ressource avec la connexion, utiliser le cache en cas d\'échec (optimise la <b>fraicheur du contenu</b>)',

	'label_images_maxsize' => 'Taille maxi (ko) des medias mises en cache',
	'label_image_fallback' => 'Image de fallback (affichée hors connexion à la place d\'une image qui n\'est pas en cache)',

	'label_cacher_ressources' => 'Ressources supplémentaires à mettre en cache',

);
