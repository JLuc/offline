<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'offline_description' => 'Ajouter facilement des capacités de consultation hors-ligne sur un site SPIP',
	'offline_nom' => 'Offline',
	'offline_slogan' => 'Permettre la consultation hors-ligne',
);
