<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'offline_titre' => 'Offline',

	'offline_info_telecharger_lecture_offline_active' => "Télécharger pour lire hors-ligne",
	'offline_info_telecharger_lecture_offline_unactive' => "Téléchargement non disponible",

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	'erreur_contenu_non_disponible' => 'Contenu non disponible hors-ligne. Connectez-vous et rechargez la page pour y accéder.',
	'info_offline_https_requis' => 'Pour utiliser le mode offline, votre site doit être configuré pour utiliser <b>https</b>. Le mode offline ne fonctionnera que sur les pages en <b>https</b>.',
	'info_offline_mode_debug_actif' => 'Le mode <b>DEBUG</b> est activé.',

	// T
	'titre_page_configurer_offline' => 'Configuration du mode Offline',
);
