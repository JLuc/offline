<?php
/**
 * Trouver les images de background dans une css
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Inc
 */


/**
 * Parser du contenu html pour trouver les ressources necessaires
 * @param $html
 * @return array
 */
function inc_offline_ressources_from_parse_css_dist($html, $url_base) {
	if (!function_exists('offline_url_normalise')) {
		include_spip('inc/offline');
	}

	$ressources = array();

	$html = explode('url', $html);
	array_shift($html);
	foreach ($html as $h) {
		if (preg_match(",^\s*\(\s*['\"]?([^'\"\s].*)['\"]?\s*\),Uims", $h, $m)) {
			$ressources[] = offline_url_normalise($m[1], $url_base);
		}
	}

	$ressources = array_filter($ressources);
	return $ressources;
}
