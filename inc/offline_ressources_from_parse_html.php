<?php
/**
 * Trouver les ressources css, js, images d'une page html
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Inc
 */


/**
 * Parser du contenu html pour trouver les ressources necessaires
 * @param $html
 * @return array
 */
function inc_offline_ressources_from_parse_html_dist($html, $url_base) {
	if (!function_exists('extraire_balises')) {
		include_spip('inc/filtres');
	}
	if (!function_exists('offline_url_normalise')) {
		include_spip('inc/offline');
	}

	if ($base = extraire_balise($html,'base')) {
		$url_base = extraire_attribut($base, 'href');
	}

	$search = array(
		'link' => 'href',
		'script' => 'src',
		'img' => 'src'
	);
	// compléter la liste des couples balise/attribut avec le contenu du define
	if (defined('_OFFLINE_RESSOURCES_TAGS') and is_array(_OFFLINE_RESSOURCES_TAGS)){
		$search = array_merge($search, _OFFLINE_RESSOURCES_TAGS);
	}

	$ressources = array();
	foreach ($search as $balise => $attribut) {

		$balises = extraire_balises($html, $balise);
		$extraire = 'offline_extraire_'.$balise.'_'.$attribut;
		if (!function_exists($extraire)) {
			$extraire = 'extraire_attribut';
		}
		foreach ($balises as $b) {
			if ($url = trim($extraire($b, $attribut))) {
				$ressources[] = offline_url_normalise($url, $url_base);
			}
		}
	}

	if (strpos($html, 'background')!==false) {
		$hb = explode('background', $html);
		array_shift($hb);
		while ($hb) {
			$h = array_shift($hb);
			if (preg_match(",\s*:[^;]*url\(([^)]*)\),", $h, $m)) {
				$url = trim($m[1]);
				$url = trim($url, "'\"");
				$url = trim($url);
				$ressources[] = offline_url_normalise($url, $url_base);
			}
		}
	}

	$ressources = array_filter($ressources);
	return $ressources;
}

function offline_extraire_link_href($link, $attribut) {
	$rel = extraire_attribut($link, 'rel');
	if ($rel == 'stylesheet') {
		return extraire_attribut($link, 'href');
	}
	return '';
}