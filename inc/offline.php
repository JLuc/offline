<?php
/**
 * Fonctions utiles
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Inc
 */

// cette constante est pris en compte dans le nom du cache
// la changer pour invalider tous les caches existants et forcer un refresh du serviceworker
if (!defined('_OFFLINE_CACHE_VERSION')) {
	define('_OFFLINE_CACHE_VERSION', 0);
}

// cette constante est pris en compte dans le nom du cache
// la changer pour invalider tous les caches existants et forcer un refresh du serviceworker
if (!defined('_OFFLINE_DEBUG')) {
	define('_OFFLINE_DEBUG', false);
}

/**
 * calculer le hash du cache qui determine sa version
 * @param $version_editoriale
 * @return string
 */
function offline_cache_hash($version_editoriale) {
	$s = "offline-" . _OFFLINE_CACHE_VERSION
		.":" . "edito-" . $version_editoriale
	;
	return substr(md5($s),0,8);
}

/**
 * Retrouver le numero de version du fichier statique
 * @param $filename
 * @return array
 */
function offline_last_service_version($filename) {
	$extension = explode('.', $filename);
	$extension = end($extension);
	$filebase = substr($filename, 0, -strlen(".$extension"));

	$version = 'X';
	// utiliser la derniere ligne ajoutee au fichier
	if ($content = file_get_contents($filename)) {
		$content = explode("\n", rtrim($content));
		$content = end($content);
		$content = explode('=', $content);
		$v = intval(end($content));

		$filename_version = "$filebase.$v.$extension";
		// verifier que les m5file correspondent bien, sinon c'est une version inconnue
		// du coup on incremente pour que ca corresponde a un fichier qui n'existe pas...
		if (file_exists($filename_version)
		  and md5_file($filename) === md5_file($filename_version)) {
			$version = $v;
		}
	}
	$filename_version = "$filebase.$version.$extension";
	return array($version, $filename_version);
}

/**
 * Ecrire le fichier statique du service en versionnant a cote les fichiers
 * ce qui permet d'avoir un historique date de ce qu'on a envoyé aux utilisateurs
 * @param string $filename
 * @param string $contenu
 */
function offline_ecrire_fichier_statique_versionne($filename, $contenu) {
	$service = basename($filename);
	$extension = explode('.', $filename);
	$extension = end($extension);
	$filebase = substr($filename, 0, -strlen(".$extension"));

	list($version, $filename_version) = offline_last_service_version($filename);
	// tester si un fichier existe et si la nouvelle version est différente ou non
	if (file_exists($filename)) {
		$old_version = file_get_contents($filename);
		$offline_version_code = "\noffline['$service']=$version;";
		if ($old_version === $contenu . $offline_version_code) {
			// rien a faire : la nouvelle version est identique au fichier existant
			return;
		}
	}

	// nouvelle version :
	$version = intval($version) + 1;
	while (file_exists($filename_version = "$filebase.$version.$extension")) {
		$version++;
	}

	$offline_version_code = "\noffline['$service']=$version;";
	ecrire_fichier($filename_version, $contenu . $offline_version_code);
	@copy($filename_version, $filename);
}

function offline_url_base_sw() {
	return $GLOBALS['meta']['adresse_site'] . '/';
}

/**
 * Retrouver l'URL relative du site (ou rien si ca n'en est pas une)
 * @param $url
 * @return bool|string
 */
function offline_url_publique_relative($url) {
	if (strpos($url, '//') === 0) {
		$url = 'https:' . $url;
	}
	if (preg_match(',^\w+://,', $url)) {
		$s = $GLOBALS['meta']['adresse_site'] . '/';
		if (strpos($url, $s) === 0) {
			return substr($url, strlen($s));
		}
		$s = offline_url_base_sw();
		if (strpos($url, $s) === 0) {
			return substr($url, strlen($s));
		}
		$s = url_absolue(_DIR_RACINE);
		if (strpos($url, $s) === 0) {
			return substr($url, strlen($s));
		}
		return '';
	}
	return $url;
}


/**
 * Normaliser/nettoyer une URL a mettre en cache issue du parsing d'autres URLS
 * ignorer les url base64
 * retirer les ancres
 * normaliser les &
 * remettre le protocole si besoin
 * passer en url absolue si besoin
 *
 * @param string $url
 * @param string $url_base
 * @return string
 */
function offline_url_normalise($url, $url_base = '') {
	$url = trim($url);
	if (strpos($url, ':') !== false
		and preg_match(',^\w+:,', $url, $m)
		and !in_array($m[0],array('http:', 'https:'))){
		return '';
	}
	if (strpos($url, "spip.php?action=cron") !== false) {
		return '';
	}
	if(strpos($url,'&amp;') !== false) {
		$url = str_replace('&amp;', '&', $url);
	}
	if (strpos($url, '#') !== false) {
		$url = explode('#', $url, 2);
		$url = reset($url);
	}
	if (strncmp($url, '//', 2) === 0) {
		$parts = parse_url($url_base ? $url_base : url_de_base());
		$url = $parts['scheme'] . ':' . $url;
	}
	elseif(strpos($url,'://')===false) {
		// les URLs relatives sont toujours par rapport a la racine du site
		// vu qu'on ne sait pas faire du multidomaine, on prend offline_url_base_sw() comme reference pour etre propre et tout le temps pareil
		if (!$url_base) {
			$url_base = offline_url_base_sw();
		}
		$url = url_absolue($url, $url_base);
	}
	return $url;
}

/**
 * URL de la 404 offline (issue du formulaire de configuration ou par défaut)
 * @return string
 */
function offline_url_404_offline() {
	if (!isset($GLOBALS['meta']['offline']) or
	  !$c = unserialize($GLOBALS['meta']['offline'])) {
		$c = array();
	}

	$url_offline_404 = '';
	if (isset($c['url_offline_404']) and $c['url_offline_404']) {
		$url_offline_404 = offline_url_publique_relative($c['url_offline_404']);
	}
	if (!$url_offline_404) {
		$url_offline_404 = generer_url_public('404_offline');
	}

	return $url_offline_404;
}

/**
 * Nettoyer une liste d'URLs issue d'une liste texte
 * @param string|array $urls
 * @return array
 */
function offline_clean_urls_list($urls) {
	if (is_string($urls)) {
		$urls = explode("\n", $urls);
	}
	$urls = array_map('trim', $urls);
	$urls = array_filter($urls);
	$urls = array_unique($urls);

	return $urls;
}

/**
 * URLs a mettre en cache
 * - soit liste des URLs configurées, par defaut
 * - soit liste buildée des URLs + de leurs ressources trouvées par parsing
 * @param bool $build
 * @return array
 */
function offline_urls_to_cache($build = false) {

	$file_urls_load = _DIR_ETC . 'offline/'. "urls_to_cache.txt";
	if ($build and file_exists($file_urls_load)) {
		$urls = file_get_contents($file_urls_load);
		$urls = offline_clean_urls_list($urls);
		return $urls;
	}

	// si pas la liste buildee demandee, ou si pas diponible on renvoi juste les urls de base
	// ie la liste source, qui devra etre buildee donc

	if (!isset($GLOBALS['meta']['offline']) or
	  !$c = unserialize($GLOBALS['meta']['offline'])) {
		$c = array();
	}

	$urls = array(
		find_in_path("img/fallback.png"),
		url_absolue(_DIR_RACINE),
		offline_url_404_offline(),
	);

	if (isset($c['cacher_ressources'])) {
		$u = offline_clean_urls_list($c['cacher_ressources']);
		$urls = array_merge($urls, $u);
	}

	return $urls;
}

/**
 * Calcul de la variable de configuration
 * @param bool $refresh
 * @return array
 */
function offline_config_js($refresh = false) {
	static $config;
	if (is_null($config) or $refresh) {
		$config = array();
		if (!isset($GLOBALS['meta']['offline']) or
		  !$c = unserialize($GLOBALS['meta']['offline'])) {
			$c = array();
		}


		$config['swUrl'] = offline_url_publique_relative(url_absolue(_DIR_RACINE  . "offline.api.sw.js", offline_url_base_sw()));
		$config['swOptions'] = array();
		$config['cacheName'] = 'offline-' . offline_cache_hash(isset($c['version_cache_edito']) ? $c['version_cache_edito'] : '');
		$config['debug'] = (defined('_OFFLINE_DEBUG') ? _OFFLINE_DEBUG : false);

		/*
		_NETWORK_ONLY: 1,
		_NETWORK_FALLING_BACK_TO_CACHE: 2,
		_CACHE_FALLING_BACK_TO_NETWORK: 4,
		_STALE_WHILE_REVALIDATE: 8,
		 */

		// par defaut network-first pour la mise a jour des contenus
		$config['strategyNav'] = 2;
		if (isset($c['strategie_cache_nav']) and $c['strategie_cache_nav']=='cache_first') {
			$config['strategyNav'] = 8; // cache first avec mise a jour en background
		}

		// par defaut cache first avec mise a jour en background
		// les ressources timestampees passeront en cache first simple, sans requete reseau si en cache
		$config['strategyRes'] = 8;
		if (isset($c['strategie_cache_res']) and $c['strategie_cache_res']=='network_first') {
			$config['strategyRes'] = 2;
		}

		$config['mediaMaxCacheSize'] = 0;
		if (isset($c['images_maxsize']) and intval($c['images_maxsize'])) {
			$config['mediaMaxCacheSize'] = 1024 * intval($c['images_maxsize']);
		}


		/*
		 * URLS a mettre en cache
		 */
		$config['urlsToCache'] = offline_urls_to_cache(true);

		$config['urlsFallback'] = array(
			'image' => find_in_path("img/fallback.png"),
			'default' => offline_url_404_offline(),
		);

	}
	return $config;
}

/**
 * Concatener une liste de script en les prefixant de la variable config
 * @param array $config
 * @param array $scripts
 * @return string
 */
function offline_build_jslist($config, $scripts) {
	$script = "var offlineConfig=" . json_encode($config) . ";\n";
	foreach ($scripts as $f) {
		if ($script_file = find_in_path($f)) {
			$script .= file_get_contents($script_file) . "\n";
		}
	}
	return $script;
}

/**
 * Lister toutes les ressources d'une URL
 * (l'url concernee inclue)
 * @param $url
 * @return mixed
 */
function offline_ressources_from_url($url, $force_refresh = false, $profondeur = 5) {
	if (is_array($url)) {
		if ($profondeur<=0) {
			return $url;
		}
		$r = array();
		foreach ($url as $u) {
			$r = array_merge($r, offline_ressources_from_url($u, $force_refresh, $profondeur));
		}
		$r = array_unique($r);
		$r = array_filter($r);
		return $r;
	}

	$url = offline_url_normalise($url);
	spip_log("offline_ressources_from_url : $url","offline");
	if (!$url) {
		return array();
	}

	$ressources = array();

	// trouver les ressources de l'URL si c'est un contenu qu'on sait parser
	$parts = parse_url($url);
	$extension = 'php';
	if (isset($parts['path'])
	  and $parts['path']
	  and preg_match(",\.(\w+)$,", $parts['path'], $m)) {
		$extension = $m[1];
	}

	$offline_ressources_from_parse = false;

	//var_dump("$profondeur:$url", $offline_ressources_from_parse);
	$delai_cache = 86400;
	if ($force_refresh) {
		$delai_cache = 0;
	}
	include_spip('inc/distant');
	$res = recuperer_url_cache($url, array('delai_cache' => $delai_cache));
	#echo "$url : " . $res['status'] . "\n";

	if (!$res or !isset($res['status']) or $res['status']>=300) {
		return $ressources;
	}
	$ressources[] = $url;

	if ($profondeur<=0) {
		return $ressources;
	}

	// parser le contenu si on sait faire

	// si c'est un spip.php on regarde le content-type pour savoir le type final
	// si on trouve pas on essaye de reconnaitre du html...
	if ($extension == 'php'){
		if ($res and isset($res['headers'])){
			if (preg_match(',Content-Type:\s*(\w+/\w+),', $res['headers'], $m)
				and $mime = $m[1]
				and $extension = sql_getfetsel('extension', 'spip_types_documents', 'mime_type=' . sql_quote($mime))){
			}
		}
		// en dernier recours si ca ressemble a du html essayons !
		if ($extension==='php'
			and isset($res['page'])
			and strpos($res['page'], "<!DOCTYPE")!==false
			and strpos($res['page'], "<html")!==false
			and strpos($res['page'], "</html>")!==false){
			$extension = 'html';
		}
	}
	$offline_ressources_from_parse = charger_fonction('offline_ressources_from_parse_' . $extension, 'inc', true);

	if ($offline_ressources_from_parse and $res and $res['page']) {
		$sub_ressources = $offline_ressources_from_parse($res['page'], $url);
		if ($sub_ressources) {
			$sub_ressources = array_diff($sub_ressources, $ressources);
			$sub_ressources = offline_ressources_from_url($sub_ressources, $force_refresh, $profondeur-1);
		}
		$ressources = array_merge($ressources, $sub_ressources);
	}
	return $ressources;
}


/**
 * Builder les services :
 * - construire la liste des URLs a télécharger à l'installation
 * - générer les fichiers js statiques qui seront servis par les /offline.api.xxxxx.js
 * (install, service, uninstall)
 *
 * @param bool $force_refresh
 *   forcer la mise a jour des URLs avant leur parsing
 * @param int|null $time_out
 *   interrompre le buil si ce timestamp est dépassé (build incrémental)
 * @return bool
 *   build terminé (true) ou non (false)
 */
function offline_build_services($force_refresh=false, $time_out=null) {

	$dir_config = sous_repertoire(_DIR_ETC, 'offline');
	$file_urls_load = $dir_config . "urls_to_cache.txt";
	$file_in_progress = $dir_config . "building.tmp";
	@touch($file_in_progress);

	$file_urls_load_progress = $file_urls_load . '.tmp';
	if (!file_exists($file_urls_load_progress)) {

		$urls_to_load = array(
			0 => offline_urls_to_cache(false)
		);
		spip_log("offline_build_services:urls_to_load" . var_export($urls_to_load, true), 'offline');
		ecrire_fichier($file_urls_load_progress, json_encode($urls_to_load));

	}

	$urls_to_load = offline_build_urls($file_urls_load_progress, $force_refresh, $time_out);
	if ($urls_to_load === false) {
		// on a pas fini, on reviendra
		return false;
	}
	@unlink($file_urls_load_progress);

	$urls_to_load = implode("\n", $urls_to_load);
	ecrire_fichier($file_urls_load, $urls_to_load);

	$config = offline_config_js();
	if ($config['debug']) {
		@unlink($file_in_progress);
		return true;
	}

	// recontruire le cache du serviceworker ( ne fera rien si on est en debug)
	$api_offline = charger_fonction('api_offline', 'action');
	$api_offline('install.js', true);
	$api_offline('sw.js', true);
	$api_offline('uninstall.js', true);

	@unlink($file_in_progress);
	return true;
}

/**
 * Construire les services, en tache de fond
 * @param bool $force_refresh
 * @param int $iteration
 */
function offline_background_build_services($force_refresh=false, $iteration=1) {

	$time_out = $_SERVER['REQUEST_TIME'] + 20;
	spip_log("offline_background_build_services iteration $iteration", 'offline');
	if (!offline_build_services($force_refresh, $time_out)) {
		spip_log("offline_background_build_services iteration $iteration : pas finie, on relance", 'offline');
		// si pas fini il faut relancer
		$iteration++;
		job_queue_add('offline_background_build_services', "Build Services offline (iter $iteration)", array($force_refresh, $iteration), 'inc/offline', true, time(), -1);
	}
	spip_log("offline_background_build_services iteration $iteration : finie !", 'offline');
}

/**
 * Construire la liste des URLs a telecharger (y compris donc les ressources), pour rendre une liste de pages consultables hors ligne
 * La construction se base sur un fichier de progression qui contient, par index de profondeur, les URLs a explorer et dans un index all toutes les URLs a telecharger in fine
 *
 * @param $file_urls_load_progress
 *   nom du fichier temporaire qui a été initalisé avec en index 0 les pages a rendre consultables hors ligne
 * @param bool $force_refresh
 *   forcer ou non la mise a jour des pages pour calculer les dependances
 * @param null $timeout
 *   interrompre le buil si ce timestamp est dépassé (build incrémental)
 * @param int $profondeur_maxi
 *   profondeur d'exploration des URLs pour trouver les ressources
 * @return array|bool
 *   liste des URLs buildées si fini, ou false si travail en cours
 */
function offline_build_urls($file_urls_load_progress, $force_refresh=false, $timeout=null, $profondeur_maxi = 5) {

	//spip_log("offline_build_urls: $file_urls_load_progress", 'offline');
	lire_fichier($file_urls_load_progress, $c);
	if (!$c or !$urls_to_load = json_decode($c, true)) {
		@unlink($file_urls_load_progress);
		return array();
	}

	if (!isset($urls_to_load['all'])) {
		$urls_to_load['all'] = array();
	}

	$i = 0;
	while(count($urls_to_load)>1 and $i++<$profondeur_maxi) {
		foreach ($urls_to_load as $profondeur => $urls) {
			if ($profondeur !== 'all') {
				//var_dump("Profondeur:$profondeur",$urls_to_load);
				$next = $profondeur + 1;
				if ($next == $profondeur_maxi) {
					$next = 'all';
				}
				if (count($urls) and !isset($urls_to_load[$next])) {
					$urls_to_load[$next] = array();
				}
				foreach ($urls as $k => $url) {
					if (in_array($url, $urls_to_load['all'])) {
						unset($urls_to_load[$profondeur][$k]);
					}
					else {
						$new_urls = offline_ressources_from_url($url, $force_refresh, 1);
						$url = array_shift($new_urls);
						$urls_to_load['all'][] = $url;
						unset($urls_to_load[$profondeur][$k]);
						$new_urls = array_unique($new_urls);
						$new_urls = array_filter($new_urls);
						while (count($new_urls) and $u = array_shift($new_urls)) {
							if (!in_array($u, $urls_to_load[$next])) {
								$urls_to_load[$next][] = $u;
							}
						}
					}

					if ($timeout and time()>$timeout) {
						ecrire_fichier($file_urls_load_progress, json_encode($urls_to_load));
						return false;
					}
				}
				unset($urls_to_load[$profondeur]);
			}
		}
	}

	$urls = $urls_to_load['all'];
	$urls = array_filter($urls);
	$urls = array_unique($urls);
	//spip_log("offline_build_urls resultat : " . var_export($urls, true), 'offline');
	return $urls;
}

/**
 * Nom du fichier qui contient la liste des URLs a telecharger pour un objet
 * @param string $objet
 * @param int $id_objet
 * @return string
 */
function offline_filename_urls_to_load_objet($objet, $id_objet) {
	$dir_config = sous_repertoire(_DIR_ETC, 'offline');
	$dir_config = sous_repertoire($dir_config, 'objets');

	// securité
	$objet = objet_type($objet);
	$id_objet = intval($id_objet);

	$file_urls_load = $dir_config . "urls-{$objet}-{$id_objet}.txt";
	return $file_urls_load;
}


/**
 * Liste des URLs a telecharger pour permettre la consultation offline d'un objet
 * @param string $objet
 * @param int $id_objet
 * @param bool $build
 *   lancer le build si il n'existe pas (true) ou pour mise a jour ('refresh')
 * @return array|bool
 */
function offline_urls_to_load_objet($objet, $id_objet, $build = false) {

	$file_urls_load = offline_filename_urls_to_load_objet($objet, $id_objet);
	if (!file_exists($file_urls_load) or !$urls = file_get_contents($file_urls_load)) {
		// Lancer un buil si besoin
		if ($build) {
			offline_start_build_urls_objet($objet, $id_objet, _VAR_MODE ? true : false);
		}
		return false;
	}

	if ($build === 'refresh') {
		offline_start_build_urls_objet($objet, $id_objet, _VAR_MODE ? true : false);
	}

	$urls = offline_clean_urls_list($urls);
	return $urls;
}

/**
 * Construite la liste des URLs a telecharger pour un objet
 * @param string $objet
 * @param int $id_objet
 * @param bool $force_refresh
 *   forcer ou non la mise a jour des pages avant de parser leur contenu
 * @param null $time_out
 *   limite de temps (build incremental) ou non (buil en cli)
 * @return array|bool|string
 */
function offline_build_urls_objet($objet, $id_objet, $force_refresh=false, $time_out=null) {

	$file_urls_load = offline_filename_urls_to_load_objet($objet, $id_objet);
	$file_urls_load_progress = $file_urls_load . '.tmp';

	if (!file_exists($file_urls_load_progress)) {

		$primary = id_table_objet($objet);
		$urls = recuperer_fond('offline/urls-'.$objet, array($primary => $id_objet));
		$urls = offline_clean_urls_list($urls);

		$urls_to_load = array(
			0 => $urls
		);
		ecrire_fichier($file_urls_load_progress, json_encode($urls_to_load));

	}

	if (!$urls_to_load = offline_build_urls($file_urls_load_progress, $force_refresh, $time_out)) {
		// on a pas fini, on reviendra
		return false;
	}
	@unlink($file_urls_load_progress);

	ecrire_fichier($file_urls_load, implode("\n", $urls_to_load));

	return $urls_to_load;
}

/**
 * Construire la liste d'URLs pour un objet, en tache de fond
 * @param string $objet
 * @param int $id_objet
 * @param bool $force_refresh
 * @param int $iteration
 */
function offline_background_build_urls_objet($objet, $id_objet, $force_refresh=false, $iteration=1) {

	$time_out = $_SERVER['REQUEST_TIME'] + 20;
	if (!offline_build_urls_objet($objet, $id_objet, $force_refresh, $time_out)) {
		// si pas fini il faut relancer
		$iteration++;
		job_queue_add('offline_background_build_urls_objet', "Build URLs offline $objet-$id_objet (iter $iteration)", array($objet, $id_objet, $force_refresh, $iteration), 'inc/offline', true, time(), -1);
	}
}